
/**
 * Return all but the first x elements of a list.
 *
 * @param {number[]} list
 * @param {number} number
 */
export const drop = (list, number) => list.slice(number, Infinity);

/**
 * Return the difference between two numbers.
 *
 * @param {number} a
 * @param {number} b
 */
export const diff = (a, b) => a > b ? -(a - b) : b - a;

/**
 * Get the highest numerical value of a list of numbers.
 *
 * @param {number[]} list
 */
export const getMax = (list) => Math.max(...list);

/**
 * Increase a number by one.
 * @param {number} v
 */
export const inc = v => v + 1;
