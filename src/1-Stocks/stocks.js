import stocksData from './stocks.data'
import { diff,drop, getMax, inc } from '../utils';

/**
 * @param {number[]} data
 * @returns {number}
 */
const getMaxProfit = (data) => {
  const deltas = data
    .map(getMaxDelta)

  return getMax(deltas);
};

/**
 * Given a list of numbers, return maximum numerical value between the first number
 * and all following numbers.
 * @param {number} value
 * @param {number} idx
 * @param {number[]} list
 */
const getMaxDelta = (value, idx, list) =>
  drop(list, inc(idx))
    .reduce((maxDelta, v) => {
      if(isNaN(v)){
        /**
         * Alternatively we could just skip the value and return the manDelta, the acceptance
         * criteria was a little unclear.
         *
         * return maxDelta;
         */
        throw Error('A non-number value has been encounted.');
      }
      return Math.max(diff(value, v), maxDelta);
    }, -Infinity)

export const stocksProblemOutput = getMaxProfit(stocksData)
export default getMaxProfit;
