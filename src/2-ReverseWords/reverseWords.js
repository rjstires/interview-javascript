// 2-ReverseWords
// Instructions can be found in src/3-ReverseWords/Reverse Words Instructions.md

/**
 *
 * @param {string} sentence
 */
export const reverseWords = (sentence) => {
  if(!sentence || typeof sentence !== 'string'){
    throw Error(`reverseWords requires a string.`)
  }
  return sentence.split(' ').reverse().join(' ')
};


var message = 'find you will pain only go you recordings security the into if';
export const reverseWordsProblemOutput = reverseWords(message)
export default reverseWords;
