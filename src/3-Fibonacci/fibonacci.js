// 3-Fibonacci
// Instructions can be found in src/3-Fibonacci/Fibonacci Instructions.md

export const fibonacci = (num) => {
  if (isNaN(num) || typeof num === 'string') {
    throw Error(`fibonacci function requires a number.`);
  }

  if (num <= 1) return num;

  return fibonacci(num - 1) + fibonacci(num - 2);
}

export const fibonacciProblemOutput = fibonacci(9)
export default fibonacci;
