import { fibonacci } from "../src/3-Fibonacci/fibonacci";

describe('fibonacci()', () => {
  it('should return the fibonnaci value at n', () => {
    expect(fibonacci(0)).toBe(0);
    expect(fibonacci(1)).toBe(1);
    expect(fibonacci(2)).toBe(1);
    expect(fibonacci(3)).toBe(2);
    expect(fibonacci(4)).toBe(3);
  });

  it('should throw an error if provided NaN', () => {
    expect(() =>
      fibonacci('1')
    ).toThrow();

    expect(() =>
      fibonacci()
    ).toThrow();
  });
});
