import { reverseWords } from "../src/2-ReverseWords/reverseWords";

describe('reverseWords()', () => {
  it('should return the reversed string', () => {
    const message = `said she what that's`;
    expect(reverseWords(message)).toBe(`that's what she said`);
  });

  it('should throw an error if provided a non string value', () => {
    expect(() =>
      reverseWords([])
    ).toThrowError();
  });
});
