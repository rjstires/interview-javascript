import getMaxProfit from '../src/1-stocks/stocks';

describe('getMaxProfit()', () => {
  it('should return maximum delta', () => {
    const result = getMaxProfit([9, 1, 1, 4, 5]);
    expect(result).toBe(4);
  });

  it('should not choke on negative numbers  ', () => {
    const result = getMaxProfit([-1, 0, 1, 2, 3]);
    expect(result).toBe(4);
  });

  it('should throw an error if provided a NaN value', () => {

    expect(() => {
      getMaxProfit(['these', 'are', 'not', 'the', 'numbers', 'you', 'were', 'looking', 'for'])
    }).toThrow();
  });
})
